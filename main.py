from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import sys
sys.stdout = open('output.txt', 'w')

def splitDataToXAndY(data):
	x = data
	y = data['Class']
	del x['Class']
	return x, y

def show_matrix(df):
    plt.figure(figsize=(51, 51))
    sns.heatmap(df.corr(), vmax=1, annot=False, square=True, cmap='RdYlGn')
    plt.title('Correlation matrix between the features')
    plt.show()

def importData():
	data = pd.read_csv("./creditcard.csv")
	print(data.shape)
	data.info()
	print(data.describe())
	print(data.head())
	print(data['Class'].value_counts())
	#show_matrix(data)
	return data

def splitData(data):
	train, test = train_test_split(data, test_size=0.33)
	train = remove_data(train, 0.9)
	xTrain, yTrain = splitDataToXAndY(train)
	xTest, yTest = splitDataToXAndY(test)
	return xTrain, yTrain, xTest, yTest

def remove_data(dataframe, ratio):
    subset = dataframe.loc[dataframe.Class == 0]
    n = int(len(subset.index) * ratio)
    drop_index = np.random.choice(subset.index, n, replace=False)
    return dataframe.drop(drop_index)

def duplicate_data(dataframe, ratio):
    duplicate_rows = []
    subset = dataframe.loc[dataframe.Class == 1]
    subset_len = len(subset.index)
    n = int(subset_len * ratio)
    for _ in range(subset_len, n):
        duplicate_index = np.random.choice(subset.index, 1)
        duplicate_rows.append(subset.loc[duplicate_index])
    return dataframe.append(duplicate_rows, ignore_index=True)

def display_score(classifier, x_train, y_train, x_test, y_test, y_pred):
    print("Train score: {}, Test score {}".format(classifier.score(x_train, y_train), classifier.score(x_test, y_test)))
    target_names = ['class 0', 'class 1']
    print(classification_report(y_test, y_pred, target_names=target_names))
    print(y_test.value_counts())
    print(pd.Series(y_pred).value_counts())
    print(confusion_matrix(y_test, y_pred))
    tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
    print('tn : {}, fp : {}, fn : {}, tp {}'.format(tn, fp, fn, tp))

def computeRndForest(x_train, y_train, x_test, y_test):
	print("\n\nRND FOREST : ")
	rndForest = RandomForestClassifier(10)
	print("Training in progress")
	rndForest.fit(x_train,y_train)
	print("Training done, test in progress")
	y_pred = rndForest.predict(x_test)
	print("Test done")
	print("SCORES : ")
	display_score(rndForest, x_train, y_train, x_test, y_test, y_pred)

def computeKNN(x_train, y_train, x_test, y_test):
	print("\n\nkNeighbors : ")
	kNeighbors= KNeighborsClassifier()
	print("Training in progress")
	kNeighbors.fit(x_train,y_train)
	print("Training done, test in progress")
	y_pred = kNeighbors.predict(x_test)
	print("Test done")
	print("SCORES : ")
	display_score(kNeighbors, x_train, y_train, x_test, y_test, y_pred)

def computeMLP(x_train, y_train, x_test, y_test):
	print("\n\nMLP : ")
	MLP = MLPClassifier(50)
	print("Training in progress")
	MLP.fit(x_train,y_train)
	print("Training done, test in progress")
	y_pred = MLP.predict(x_test)
	print("Test done")
	print("SCORES : ")
	display_score(MLP, x_train, y_train, x_test, y_test, y_pred)

def computeDecisionTree(x_train, y_train, x_test, y_test):
	print("\n\nDecisionTree : ")
	decisionTree = tree.DecisionTreeClassifier()
	print("Training in progress")
	decisionTree.fit(x_train,y_train)
	print("Training done, test in progress")
	y_pred = decisionTree.predict(x_test)
	print("Test done")
	print("SCORES : ")
	display_score(decisionTree, x_train, y_train, x_test, y_test, y_pred) 

x_train, y_train, x_test, y_test = splitData(importData())
computeRndForest(x_train, y_train, x_test, y_test)
computeKNN(x_train, y_train, x_test, y_test)
computeMLP(x_train, y_train, x_test, y_test)
computeDecisionTree(x_train, y_train, x_test, y_test)
sys.stdout.close()