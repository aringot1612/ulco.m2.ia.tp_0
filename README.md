# ulco.m2.ia.tp_0

## Description

Projet étudiant M2 : IA TP-0

## Auteur

- [Ringot Arthur](https://gitlab.com/aringot1612)

## Aperçus console

<br>![Aperçu Rnd Forest](images/rndForest.png)

<br>![Aperçu kNeighbors](images/kNeighbors.png)

<br>![Aperçu MLP](images/MLP.png)